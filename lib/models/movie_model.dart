class MovieModel{
  double popularity;
  int voteCount;
  bool video;
  String posterPath;
  int id;
  bool adult;
  String backdropPath;
  String originalLanguage;
  String originalTitle;
  String title;
  double voteAverage;
  String overview;
  String releaseDate;

  MovieModel({
    this.popularity,
    this.voteCount,
    this.video,
    this.posterPath,
    this.id,
    this.adult,
    this.backdropPath,
    this.originalLanguage,
    this.originalTitle,
    this.title,
    this.voteAverage,
    this.overview,
    this.releaseDate,
  });

  factory MovieModel.fromJson(Map<String, dynamic> json){
    return MovieModel(
      popularity: json["popularity"].toDouble(),
      voteCount: json["vote_count"],
      video: json["video"],
      posterPath: json["poster_path"],
      id: json["id"],
      adult: json["adult"],
      backdropPath: json["backdrop_path"],
      originalLanguage: json["original_language"],
      originalTitle: json["original_title"],
      title: json["title"],
      voteAverage: json["vote_average"].toDouble(),
      overview: json["overview"],
      releaseDate: json["release_date"],
    );
  }

  String get getPosterImage => "https://image.tmdb.org/t/p/w500$posterPath";
  String get getBackdropImage => "https://image.tmdb.org/t/p/w500$backdropPath";
}