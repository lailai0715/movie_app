import 'package:flutter/material.dart';
import 'package:movie_app/models/movie_model.dart';
import 'package:movie_app/pages/fav_movie_list_page.dart';
import 'package:movie_app/pages/movie_detail_page.dart';
import 'package:movie_app/utils/api.dart' as api;

class MovieListPage extends StatefulWidget{
  static const String routeName = "/";
  @override
  State<StatefulWidget> createState() {
    return _HomePage();
  }
}

class _HomePage extends State<MovieListPage>{
  List<MovieModel> movieList = [];
  List<MovieModel> favMovieList =[];

  bool isLoading = false;

  @override
  void initState() {
    super.initState();
    isLoading = true;
    setState(() {});
    api.fetchMovieList().then((res){
      isLoading = false;
      if(res != null) movieList = res;
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Movie App"),
        centerTitle: true,
        actions: <Widget>[
          IconButton(
            onPressed: (){
              print(favMovieList.length);
              if(favMovieList.isEmpty) return;
              Navigator.pushNamed(context, FavMovieListPage.routeName, arguments: {"modelList": favMovieList});
            },
            icon: Icon(Icons.favorite),
          ),
        ],
      ),
      body: isLoading ? Center(child: CircularProgressIndicator(),):
          GridView.builder(
            itemCount: movieList.length,
            gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
            itemBuilder: (_, index){
              MovieModel model = movieList[index];
              return Container(
                padding: EdgeInsets.all(1),
                child: GridTile(
                  header: Container(
                    alignment: Alignment.topRight,
                    child: IconButton(
                      icon: Icon(favMovieList.contains(model) ?  Icons.favorite: Icons.favorite_border, color: Colors.red,),
                      onPressed: (){
                        if(favMovieList.contains(model)){
                          favMovieList.remove(model);
                        }else{
                          favMovieList.add(model);
                        }
                        setState(() {});
                      },
                    ),
                  ),
                  child: InkWell(
                    onTap: (){
                      Navigator.pushNamed(context, MovieDetailPage.routeName, arguments: {"model": model});
                    },
                    child: Hero(
                      tag: model.id,
                      child: Image.network(model.getPosterImage, fit: BoxFit.cover,),
                    )
                  ),
                  footer: Container(color:Colors.white, child: Text(model.title, textAlign: TextAlign.center,),),
                ),
              );
            }
          )
    );
  }
}