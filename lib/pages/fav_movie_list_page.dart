import 'package:flutter/material.dart';
import 'package:movie_app/models/movie_model.dart';

class FavMovieListPage extends StatefulWidget{
  static const String routeName = "/fav_movie_list";

  final List<MovieModel> modelList;
  FavMovieListPage({this.modelList});

  @override
  State<StatefulWidget> createState() {
    return _FavMovieListPage();
  }
}

class _FavMovieListPage extends State<FavMovieListPage>{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Fav Movies"),
        centerTitle: true,
      ),
      body: ListView.separated(
        separatorBuilder: (_, index)=>Divider(),
        itemCount: widget.modelList.length,
        itemBuilder: (_, index){
          MovieModel model = widget.modelList[index];
         return ListTile(
           leading: Image.network(model.getPosterImage, fit: BoxFit.cover,),
           title: Text(model.title),
           subtitle: Text(model.overview),
         );
        }
      ),
    );
  }
}