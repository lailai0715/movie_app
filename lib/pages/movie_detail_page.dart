import 'package:flutter/material.dart';
import 'package:movie_app/models/movie_model.dart';

class MovieDetailPage extends StatefulWidget{
  static const String routeName = "/movie_detail_page";

  final MovieModel model;
  MovieDetailPage({this.model});

  @override
  State<StatefulWidget> createState() {
    return _MovieDetailPage();
  }
}

class _MovieDetailPage extends State<MovieDetailPage>{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.model.title),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Image.network(widget.model.getBackdropImage),
            Container(
              margin: EdgeInsets.all(5),
              child: Text(widget.model.title, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),),
            ),
            Container(
              margin: EdgeInsets.all(5),
              child: Text(widget.model.overview, textAlign: TextAlign.center,),
            ),
            SizedBox(height: 20,),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Text("vote: ${widget.model.voteAverage}"),
                Text("release date: ${widget.model.releaseDate}"),
              ],
            ),
            SizedBox(height: 20,),
            Hero(
              tag: widget.model.id,
              child: Image.network(widget.model.getPosterImage, width: 250, height: 250,),
            )
          ],
        ),
      ),
    );
  }
}