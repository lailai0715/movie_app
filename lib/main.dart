import 'package:flutter/material.dart';
import 'package:movie_app/models/movie_model.dart';
import 'package:movie_app/pages/fav_movie_list_page.dart';
import 'package:movie_app/pages/movie_detail_page.dart';
import 'package:movie_app/pages/movie_list_page.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      routes: {
        MovieListPage.routeName: (_) => MovieListPage(),
      },
      onGenerateRoute: (settings){
        switch(settings.name){
          case MovieDetailPage.routeName:
            Map<String, dynamic> arg = settings.arguments;
            MovieModel model = arg["model"];

            return MaterialPageRoute<bool>(
              builder: (BuildContext context) => MovieDetailPage(model: model,)
            );
          case FavMovieListPage.routeName:
            Map<String, dynamic> arg = settings.arguments;
            List<MovieModel> model = arg["modelList"];

            return MaterialPageRoute<bool>(
              builder: (_) => FavMovieListPage(modelList: model,)
            );
        }
        return null;
      },
    );
  }
}
